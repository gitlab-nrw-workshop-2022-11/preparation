# Einführung in git und FDM mit GitLab

Datum: 10.11.2022, 09:00 - 13:00 Uhr

Format: Online, Flipped Classroom in und mit GitLab, ~1,5h Vorbereitungszeit

Technik Check: 07.11.2022 15:00 Uhr

Herzlich willkommen zur online-Vorbereitung zum Gitlab-Workshop *Einführung in git und FDM mit GitLab*

## Was ist git/Gitlab?

Git ist eine etablierte Software zur Versionsverwaltung von Dateien. Wenn auch voranging zum Code-Management in der Softwareentwicklung genutzt, können in der Praxis nahezu jede Art von Datei per Versionsverwaltung nachverfolgt werden. Gut geeignet ist git für flach strukturierte, textbasierte Datensätze wie csv, xml usw.

Die Weboberfläche GitLab ist eine Open-Source-Software, um eigene git-Repositories selbst zu hosten und zu verwalten. Neben der Hauptaufgabe des Code-Managements werden noch andere Funktionalitäten wie ein einfaches Issue-Tracking-System, Wiki sowie Code-Review-Möglichkeiten abgedeckt. GitLab wird an vielen Hochschulen lokal angeboten oder kann wie [GitLab an der RWTH Aachen](https://git-ce.rwth-aachen.de) in Kooperation genutzt werden. Um ein gemeinsames Arbeiten zu erleichetern verwenden wir im Workshop die Cloud-Variante von GitLab: [GitLab.com](https://www.gitlab.com) 

## Online-Vorbereitung

Zur vorbereitung auf den Workshop stellen wir Ihnen Lernmaterialien zur Verfügung, die Sie dazu anleiten, git auf Ihrem Rechner zu installieren, ein Projekt einzurichten und erste einfache Befehle auszuführen. Der online-Kurs gliedert sich in die Installation von git, git Basics zur Versionskontrolle und in eine Einführung in die Zusammenarbeit mit GitLab. Um alles auszuprobieren sollten Sie bis zu einen Vormittag Zeit einplanen. Sollten Sie während Ihrer online-Vorbereitung Fragen haben, können Sie diese als [Issue / Ticket](https://gitlab.com/gitlab-nrw-workshop-2022-11/preparation/-/issues) stellen. Wir werden versuchen, diese dann zeitnah zu beantworten.

Am Ende der online-Vorbereitung sollten Sie git auf Ihrem Rechner installiert, ein erstes Projekt angelegt und erste einfache Befehle mit git ausgeführt haben. Sie haben sich auf der [GitLab Web-Oberfläche](https://www.gitlab.com) eingeloggt und sollten in der Lage sein, mit GitLab loslegen zu können.

Im Workshop werden Sie weitere git/GitLab-Funktionalitäten kennenlernen. Gemeinsam werden wir im Workshop Anwendungsbeispiele diskutieren und Übungen zu individuellen Arbeitsabläufe und Best Practices durchführen. Bitte stellen Sie sicher, dass Sie während dem Workshop zugriff auf die git-Software und und die [GitLab Web-Oberfläche](https://www.gitlab.com) haben.

Um Ihren Fortschritt bei der Online-Vorbereitung zu verfolgen können Sie eine Checkliste in Gitlab erstellen. Dafür müssen Sie einen Account bei GitLab.com einrichten:

1. Anmelden bei GitLab.com
1. Neuen Arbeitsbereich für die Vorbereitung erstellen

### Anmelden bei&nbsp;[GitLab.com](https://www.gitlab.com)

Im Workshop soll gemeinsam mit den anderen Teilnehmern ausprobiert werden wie sich git und GitLab in einer kleinen Gruppe Nutzen lassen. Dafür müssen alle Teilnehmer einen Account auf einem GitLab server haben. Im workshop nutzen wir die Cloud-Version [GitLab.com](https://www.gitlab.com).

#### Erster Kontakt

Falls Sie noch keinen Account auf [GitLab.com](https://www.gitlab.com) haben müssten Sie sich zunächst registrieren. Öffenen Sie dazu die Webseite https://gitlab.com/users/sign_up und füllen Sie dort das Registrierungsformular aus oder Nutzen Sie eine der verschiedenen "Social Sign On" möglichkeiten (z.B. Google, GitHub oder Twitter).

### Arbeitsbereich für die Vorbereitung

Für die Vorbereitung sollten Sie einen eigenen Arbeitsbereich erstellen. Dort können Sie ihren individuellen Fortschritt bei der Vorbereitung nachvollziehen:

:arrow_right: [Jetzt einen neuen Arbeitsbereich erstellen](https://gitlab.com/gitlab-nrw-workshop-2022-11/preparation/-/issues/new?issuable_template=Preparation&issue[title]=Online+Vorbereitung) :arrow_left:

(oder [bestehenden Arbeitsbereich anzeigen](https://gitlab.com/gitlab-nrw-workshop-2022-11/preparation/-/issues?search=Online+Vorbereitung))

Klicken Sie auf der verlinkten Seite dafür einfach unten auf die Schaltfläche "Submit Issue" bzw. "Submit ticket".

Auf dem folgenden Bildschirm erhalten Sie eine Checkliste für die Online Vorbereitung.

### Ohne Arbeitsbereich vorbereiten

Falls Sie sich ohne Arbeitsbereich vorbereiten möchten finden Sie auch alle Inhalte hier auf der Seite in der Navigation unter dem Punkt "Online-Vorbereitung".
