# TortoiseGit

Die Installation von tortoiseGit ist optional. Um tortoiseGit installieren und verwenden zu können, benötigen Sie eine vorhandene Git Installation. Die benötigte Installationsdatei können Sie sich auf folgender Website herunterladen: https://tortoisegit.org

![](./img/1_Download_tort.jpg)

Klicken Sie auf dieser Seite auf "Download" ...

![](./img/2_Download_tort.png)

... um in den Downloadbereich zu gelangen. Wählen Sie hier die für Ihr System benötigte Datei und klicken Sie auf den entsprechenden Link.

![](./img/3_Download_tort.png)

Speichern Sie die Datei auf Ihrem Rechner.

![](./img/4_Install_tort.png)

Nach Abschluss des Downloads starten Sie die Datei per Doppelklick und bestätigen den folgenden Dialog mit "OK".

![](./img/5_Install_tort.png)

Die Installationsroutine sollte nun starten, setzen Sie sie über den Button "Next" fort.

![](./img/6_install_tort.png)

Lesen Sie die Lizenzinformationen und klicken Sie "Next".

![](./img/7_install_tort.png)

Wählen Sie in diesem Dialog den obersten Auswahlpunkt und bestätigen Sie mit "Next".

![](./img/8_install_tort.png)

Wählen Sie einen Installationsort für tortoiseGit auf Ihrer Festplatte.

![](./img/9_install_tort.png)

Starten Sie anschließend die Installation über den Button "Install".

![](./img/10_install_tort.png)

Warten Sie bis die Installation abgeschlossen ist.

![](./img/11_install_tort.png)

Setzen Sie den Haken in der Auswahlbox wie gezeigt und klicken Sie "Finish".

![](./img/12_install_tort.png)

Wählen Sie die Sprache aus, in der tortoiseGit laufen soll. Wir empfehlen Ihnen Englisch zu verwenden.

![](./img/13_install_tort.png)

Setzen Sie die Ersteinrichtung mit "Weiter" fort.

![](./img/14_install_tort.png)

Der anzugebende Pfad zu git.ext sollte automatisch eingetragen werden. Andernfalls finden Sie eine git.ext in Ihrem Git Installationsverzeichnis. Klicken Sie "Weiter".

![](./img/15_install_tort.png)

Wählen Sie auf dieser Seite "Don't store these settings now" und klicken Sie auf "Weiter".

![](./img/17_install_tort.png)

Wählen Sie auch hier "Don't store these settings now" und bestätigen Sie mit "Fertigstellen".

![](./img/18_Startmenue_tort.png)

TortoiseGit ist installiert. Sie finden es z.B. im Startmenü.
